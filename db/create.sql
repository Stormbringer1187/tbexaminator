﻿create table vaadintest_clients (
ID uuid,
CREATE_TS timestamp,
CREATED_BY varchar(50),
UPDATE_TS timestamp,
UPDATED_BY varchar(50),
DELETE_TS timestamp,
DELETED_BY varchar(50),
LAST_NAME varchar(50),
FIRST_NAME varchar(50),
PATRONYMIC varchar(50),
DEATH_CHANCE decimal(19, 2),
COMMENT varchar(5000),
primary key(ID)
)
