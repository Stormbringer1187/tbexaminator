﻿
CREATE OR REPLACE FUNCTION public.uuid_generate_v1()
  RETURNS uuid AS
'$libdir/uuid-ossp', 'uuid_generate_v1'
  LANGUAGE c VOLATILE STRICT
  COST 1;
ALTER FUNCTION public.uuid_generate_v1()
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION public.newid()
  RETURNS uuid AS
'select uuid_generate_v1();'
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION public.newid()
  OWNER TO postgres;