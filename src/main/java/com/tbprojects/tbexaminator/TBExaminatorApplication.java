package com.tbprojects.tbexaminator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TBExaminatorApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(TBExaminatorApplication.class, args);
    }
    
}