package com.tbprojects.tbexaminator.components;

import com.tbprojects.tbexaminator.domain.Examination;
import com.tbprojects.tbexaminator.enums.*;
import com.tbprojects.tbexaminator.repositories.ExaminationsRepository;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.dom.Style;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.util.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

@SpringComponent
@UIScope
public class ExaminationEditor extends VerticalLayout implements KeyNotifier {
    
    private boolean isAdmin;
    protected final ExaminationsRepository examinationsRepository;
    protected Examination examination;
    protected Examination template;
    public final String fieldWidth = "400px";
    public final static BigDecimal GOOD_MARK_CEILING = BigDecimal.valueOf(90);
    public final static BigDecimal SATISFACTORY_MARK_CEILING = BigDecimal.valueOf(80);
    public final static BigDecimal BAD_MARK_CEILING = BigDecimal.valueOf(70);
    
    @Autowired
    public ExaminationEditor(ExaminationsRepository examinationsRepository) {
        isAdmin = false;
        
        this.examinationsRepository = examinationsRepository;
    
        binder.bindInstanceFields(this);
        
        setWidthFull();
        upload.setAcceptedFileTypes("image/png", "image/jpeg");
        picture.setVisible(false);
        isTemplate.setVisible(false);
        
        state.setItems(EnumUtils.getEnumList(StateEnum.class));
        state.setItemLabelGenerator(StateEnum::toLocalizedString);
        state.setWidth(fieldWidth);
        state.setRequired(true);
        rightStateLbl.setWidth("400px");
        
        riskFactors.setItems(EnumUtils.getEnumList(RiskFactorsEnum.class));
        riskFactors.setWidth(fieldWidth);
        riskFactors.setItemLabelGenerator(RiskFactorsEnum::toLocalizedString);
        riskFactors.setRequired(true);
        rightRiskFactorsLbl.setWidth(fieldWidth);
        
        vaccinationEffect.setItems(EnumUtils.getEnumList(VaccinationEffectEnum.class));
        vaccinationEffect.setWidth(fieldWidth);
        vaccinationEffect.setItemLabelGenerator(VaccinationEffectEnum::toLocalizedString);
        vaccinationEffect.setRequired(true);
        rightVaccinationEffectLbl.setWidth(fieldWidth);
        
        ppd.setItems(EnumUtils.getEnumList(PPDEnum.class));
        ppd.setWidth(fieldWidth);
        ppd.setItemLabelGenerator(PPDEnum::toLocalizedString);
        ppd.setRequired(true);
        rightPpdLbl.setWidth(fieldWidth);
        
        immuneDiagnostic.setItems(EnumUtils.getEnumList(ImmuneDiagnosticEnum.class));
        immuneDiagnostic.setWidth(fieldWidth);
        immuneDiagnostic.setItemLabelGenerator(ImmuneDiagnosticEnum::toLocalizedString);
        immuneDiagnostic.setRequired(true);
        rightImmuneDiagnosticLbl.setWidth(fieldWidth);
        
        diaskinTest.setItems(EnumUtils.getEnumList(YesNoEnum.class));
        diaskinTest.setWidth(fieldWidth);
        diaskinTest.setItemLabelGenerator(YesNoEnum::toLocalizedString);
        diaskinTest.setRequired(true);
        rightDiaskinTestLbl.setWidth(fieldWidth);
        
        infectingAge.setItems(EnumUtils.getEnumList(AgeEnum.class));
        infectingAge.setWidth(fieldWidth);
        infectingAge.setItemLabelGenerator(AgeEnum::toLocalizedString);
        infectingAge.setRequired(true);
        rightInfectingAgeLbl.setWidth(fieldWidth);
        
        xRayDiagramSyndrome.setItems(EnumUtils.getEnumList(XRayEnum.class));
        xRayDiagramSyndrome.setWidth(fieldWidth);
        xRayDiagramSyndrome.setItemLabelGenerator(XRayEnum::toLocalizedString);
        xRayDiagramSyndrome.setRequired(true);
        rightXRayDiagramSyndromeLbl.setWidth(fieldWidth);
        
        focusIsPresent.setItems(EnumUtils.getEnumList(YesNoEnum.class));
        focusIsPresent.setWidth(fieldWidth);
        focusIsPresent.setItemLabelGenerator(YesNoEnum::toLocalizedString);
        focusIsPresent.setRequired(true);
        rightFocusIsPresentLbl.setWidth(fieldWidth);
        
        focusCategory.setItems(EnumUtils.getEnumList(FocusEnum.class));
        focusCategory.setWidth(fieldWidth);
        focusCategory.setItemLabelGenerator(FocusEnum::toLocalizedString);
        focusCategory.setRequired(true);
        rightFocusIsPresentLbl.setWidth(fieldWidth);
        
        contactPeople.setItems(EnumUtils.getEnumList(ContactPeopleEnum.class));
        contactPeople.setWidth(fieldWidth);
        contactPeople.setItemLabelGenerator(ContactPeopleEnum::toLocalizedString);
        contactPeople.setRequired(true);
        rightContactPeopleLbl.setWidth(fieldWidth);
        
        needsPhthisiologist.setItems(EnumUtils.getEnumList(YesNoEnum.class));
        needsPhthisiologist.setWidth(fieldWidth);
        needsPhthisiologist.setItemLabelGenerator(YesNoEnum::toLocalizedString);
        needsPhthisiologist.setRequired(true);
        rightNeedsPhthisiologistLbl.setWidth(fieldWidth);
        
        diagnosis.setItems(EnumUtils.getEnumList(DiagnosisEnum.class));
        diagnosis.setWidth(fieldWidth);
        diagnosis.setItemLabelGenerator(DiagnosisEnum::toLocalizedString);
        diagnosis.setRequired(true);
        rightDiagnosisLbl.setWidth(fieldWidth);
        
        isTemplate.addValueChangeListener(event -> {
           studentsLastName.setVisible(!event.getValue());
           studentsFirstName.setVisible(!event.getValue());
           studentsMiddleName.setVisible(!event.getValue());
           percentsAndMark.setVisible(!event.getValue());
        });
        
        buttons.add(saveBtn, closeBtn);
        percentsAndMark.add(result, percentLbl, mark);
        percentsAndMark.setVerticalComponentAlignment(Alignment.START, mark);
        percentsAndMark.setVerticalComponentAlignment(Alignment.END, percentLbl);
        percentLbl.setHeightFull();
        
        add(isTemplate, number, studentsLastName, studentsFirstName, studentsMiddleName,
            description, upload, picture, stateLbl, state, rightStateLbl,
            riskFactorsLbl, riskFactors, rightRiskFactorsLbl, vaccinationEffectLbl, vaccinationEffect,
            rightVaccinationEffectLbl, ppdLbl, ppd, rightPpdLbl,
            immuneDiagnosticLbl, immuneDiagnostic, rightImmuneDiagnosticLbl,
            diaskinTestLbl, diaskinTest, rightDiaskinTestLbl, infectingAgeLbl, infectingAge,
            rightInfectingAgeLbl, xRayDiagramSyndromeLbl, xRayDiagramSyndrome,
            rightXRayDiagramSyndromeLbl, focusIsPresentLbl, focusIsPresent,
            rightFocusIsPresentLbl, focusCategoryLbl,
            focusCategory, rightFocusCategoryLbl, contactPeopleLbl, contactPeople,
            rightContactPeopleLbl, needsPhthisiologistLbl,
            needsPhthisiologist, rightNeedsPhthisiologistLbl, diagnosisLbl, diagnosis,
            rightDiagnosisLbl, percentsAndMark, buttons);
        makeAnswersVisible(false);
        
        saveBtn.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
                if (getExamination() != null) {
                    if (!isExaminationFilled(getExamination())) {
                        Notification.show("Заполните все поля", 5000,
                            Notification.Position.MIDDLE).open();
                        return;
                    }
                    checkAnswers(getExamination(), getTemplate());
                    makeAnswersVisible(true);
                    save();
                    changeHandler.onChange();
                }
            }
        });
        closeBtn.addClickListener(event -> {
            close();
        });
        upload.addSucceededListener(event -> {
            if (getExamination() != null) {
                try {
                    getExamination().setImageContent(memoryBuffer.getInputStream().readAllBytes());
                    setAndShowPicture();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        setAlignItems(Alignment.BASELINE);
        description.setWidthFull();
        description.setHeight("400px");
        setVisible(false);
    }
    protected HorizontalLayout buttons = new HorizontalLayout();
    protected HorizontalLayout percentsAndMark = new HorizontalLayout();
    
    protected Image picture = new Image();
    
    protected TextField number = new TextField("Номер задания");
    
    protected TextArea description = new TextArea("Описание случая");
    
    protected MemoryBuffer memoryBuffer = new MemoryBuffer();
    protected Upload upload = new Upload(memoryBuffer);
    
    protected Checkbox isTemplate = new Checkbox("Шаблон задания");
    
    protected TextField studentsLastName = new TextField("Фамилия студента");
    protected TextField studentsFirstName = new TextField("Имя студента");
    protected TextField studentsMiddleName = new TextField("Отчество студента");
    
    protected Label stateLbl = new Label("1. Оцените общее состояние");
    protected ComboBox<StateEnum> state = new ComboBox<>();
    protected Label rightStateLbl = new Label();
    
    protected Label riskFactorsLbl = new Label("2. Выделите факторы риска по туберкулезу");
    protected CheckboxGroup<RiskFactorsEnum> riskFactors = new CheckboxGroup<>();
    protected Label rightRiskFactorsLbl = new Label();
    
    protected Label vaccinationEffectLbl = new Label("3 Оцените эффективность вакцинации");
    protected ComboBox<VaccinationEffectEnum> vaccinationEffect = new ComboBox<>();
    protected Label rightVaccinationEffectLbl = new Label();
    
    protected Label ppdLbl = new Label("4. Оцените последний результат пробы Манту с 2ТЕ");
    protected ComboBox<PPDEnum> ppd = new ComboBox<>();
    protected Label rightPpdLbl = new Label();
    
    protected Label immuneDiagnosticLbl = new Label("5 Оцените данные иммунодиагностики по годам");
    protected ComboBox<ImmuneDiagnosticEnum> immuneDiagnostic = new ComboBox<>();
    protected Label rightImmuneDiagnosticLbl = new Label();
    
    protected Label diaskinTestLbl = new Label("6. Проба Диаскин-тест - инфекционная?");
    protected ComboBox<YesNoEnum> diaskinTest = new ComboBox<>();
    protected Label rightDiaskinTestLbl = new Label();
    
    protected Label infectingAgeLbl = new Label("7. Укажите возраст когда, произошло " +
        "инфицирование");
    protected ComboBox<AgeEnum> infectingAge = new ComboBox<>();
    protected Label rightInfectingAgeLbl = new Label();
    
    protected Label xRayDiagramSyndromeLbl = new Label("8. Выделите рентгенологический синдром на " +
        "рентгенограмме");
    protected ComboBox<XRayEnum> xRayDiagramSyndrome = new ComboBox<>();
    protected Label rightXRayDiagramSyndromeLbl = new Label();
    
    protected Label focusIsPresentLbl = new Label("9. Можно ли в данном случае говорить об очаге" +
        " туберкулеза?");
    protected ComboBox<YesNoEnum> focusIsPresent = new ComboBox<>();
    protected Label rightFocusIsPresentLbl = new Label();
    
    protected Label focusCategoryLbl = new Label("10. Какая категория очага?");
    protected ComboBox<FocusEnum> focusCategory = new ComboBox<>();
    protected Label rightFocusCategoryLbl = new Label();
    
    protected Label contactPeopleLbl = new Label("11. Сколько человек являются контактными?");
    protected ComboBox<ContactPeopleEnum> contactPeople = new ComboBox<>();
    protected Label rightContactPeopleLbl = new Label();
    
    protected Label needsPhthisiologistLbl = new Label("12. Надо ли направить ребенка к " +
        "фтизиатру?");
    protected ComboBox<YesNoEnum> needsPhthisiologist = new ComboBox<>();
    protected Label rightNeedsPhthisiologistLbl = new Label();
    
    protected Label diagnosisLbl = new Label("13. Поставьте предварительный клинический диагноз");
    protected ComboBox<DiagnosisEnum> diagnosis = new ComboBox<>();
    protected Label rightDiagnosisLbl = new Label();
    
    protected BigDecimalField result = new BigDecimalField("Результат");
    protected Label percentLbl = new Label("%");
    
    protected IntegerField mark = new IntegerField("Оценка");
    
    protected Button saveBtn = new Button("Готово");
    
    protected Button closeBtn = new Button("Закрыть");
    
    protected Binder<Examination> binder = new Binder<>(Examination.class);
    
    protected ChangeHandler changeHandler;
    
    public void setChangeHandler(ChangeHandler changeHandler) {
        this.changeHandler = changeHandler;
    }
    
    public interface ChangeHandler {
        void onChange();
    }
    
    public void save() {
        if (examination.getDeleteTs() == null) {
            if (examination.getCreateTs() != null) {
                examination.setUpdateTs(Date.from(Instant.now()));
            } else {
                examination.setCreateTs(Date.from(Instant.now()));
            }
        }
        examinationsRepository.save(examination);
    }
    
    public void close() {
        changeHandler.onChange();
        setVisible(false);
    }
    
    public void editExamination(Examination examination, Examination template, boolean isAdmin) {
        this.isAdmin = isAdmin;
        
        if (examination != null) {
            if (template != null) {
                setTemplate(template);
                examination.setImageContent(template.getImageContent());
                examination.setNumber(template.getNumber());
                examination.setDescription(template.getDescription());
            }
            boolean savedExamination = examination.getCreateTs() != null;
            setExamination(examination);
            upload.setVisible(isAdmin);
            number.setEnabled(isAdmin && (!savedExamination || examination.getIsTemplate()));
            isTemplate.setVisible(isAdmin);
            description.setEnabled(isAdmin && (!savedExamination || examination.getIsTemplate()));
            setAndShowPicture();
            binder.setBean(getExamination());
            setVisible(true);
        }
        
    }
    
    public void checkAnswers(Examination examination, Examination template) {
        if (template != null) {
            int points = 0;
            int maxPointsCount = 0;
            String rightAnswerTemplate = "Правильный ответ: ";
            BigDecimal percents;
            Map<Component, Boolean> resultMap = new HashMap<>();
            resultMap.put(contactPeople,
                StringUtils.equals(template.getContactPeople(), examination.getContactPeople()));
            resultMap.put(diagnosis, StringUtils.equals(template.getDiagnosis(),
                examination.getDiagnosis()));
            resultMap.put(state, StringUtils.equals(template.getState(), examination.getState()));
            resultMap.put(riskFactors, StringUtils.equals(template.getRiskFactors(),
                examination.getRiskFactors()));
            resultMap.put(vaccinationEffect, StringUtils.equals(template.getVaccinationEffect(),
                examination.getVaccinationEffect()));
            resultMap.put(ppd, StringUtils.equals(template.getPpd(), examination.getPpd()));
            resultMap.put(immuneDiagnostic, StringUtils.equals(template.getImmuneDiagnostic(),
                examination.getImmuneDiagnostic()));
            resultMap.put(diaskinTest, StringUtils.equals(template.getDiaskinTest(),
                examination.getDiaskinTest()));
            resultMap.put(infectingAge, StringUtils.equals(template.getInfectingAge(),
                examination.getInfectingAge()));
            resultMap.put(xRayDiagramSyndrome,
                StringUtils.equals(template.getXRayDiagramSyndrome(),
                    examination.getXRayDiagramSyndrome()));
            resultMap.put(focusIsPresent, StringUtils.equals(template.getFocusIsPresent(),
                examination.getFocusIsPresent()));
            resultMap.put(focusCategory, StringUtils.equals(template.getFocusCategory(),
                examination.getFocusCategory()));
            resultMap.put(needsPhthisiologist,
                StringUtils.equals(template.getNeedsPhthisiologist(),
                    examination.getNeedsPhthisiologist()));
            for (Boolean value : resultMap.values()) {
                if (value) {
                    points++;
                }
                maxPointsCount++;
            }
            percents = (BigDecimal.valueOf(points)
                .divide(BigDecimal.valueOf(maxPointsCount), 2, RoundingMode.HALF_EVEN))
                .multiply(BigDecimal.valueOf(100)).setScale(0, RoundingMode.HALF_EVEN);
            examination.setResult(percents);
            if (percents.compareTo(BAD_MARK_CEILING) < 0) {
                examination.setMark(2);
            } else if (percents.compareTo(SATISFACTORY_MARK_CEILING) < 0) {
                examination.setMark(3);
            } else if (percents.compareTo(GOOD_MARK_CEILING) < 0) {
                examination.setMark(4);
            } else {
                examination.setMark(5);
            }
            rightStateLbl.getElement().setText(rightAnswerTemplate +
                template.getState().toLocalizedString());
            StringBuilder riskFactorsString = new StringBuilder();
            Iterator<RiskFactorsEnum> iterator = template.getRiskFactors().iterator();
            while (iterator.hasNext()) {
                RiskFactorsEnum factor = iterator.next();
                riskFactorsString.append(factor.toLocalizedString());
                if (iterator.hasNext()) {
                    riskFactorsString.append(", ");
                }
            }
            rightRiskFactorsLbl.setText(rightAnswerTemplate + riskFactorsString.toString());
            rightVaccinationEffectLbl.setText(rightAnswerTemplate +
                template.getVaccinationEffect().toLocalizedString());
            rightPpdLbl.setText(rightAnswerTemplate + template.getPpd().toLocalizedString());
            rightImmuneDiagnosticLbl.setText(rightAnswerTemplate +
                template.getImmuneDiagnostic().toLocalizedString());
            rightDiaskinTestLbl.setText(rightAnswerTemplate +
                template.getDiaskinTest().toLocalizedString());
            rightInfectingAgeLbl.setText(rightAnswerTemplate +
                template.getInfectingAge().toLocalizedString());
            rightXRayDiagramSyndromeLbl.setText(rightAnswerTemplate +
                template.getXRayDiagramSyndrome().toLocalizedString());
            rightFocusIsPresentLbl.setText(rightAnswerTemplate +
                template.getFocusIsPresent().toLocalizedString());
            rightFocusCategoryLbl.setText(rightAnswerTemplate +
                template.getFocusCategory().toLocalizedString());
            rightContactPeopleLbl.setText(rightAnswerTemplate +
                template.getContactPeople().toLocalizedString());
            rightNeedsPhthisiologistLbl.setText(rightAnswerTemplate +
                template.getNeedsPhthisiologist().toLocalizedString());
            rightDiagnosisLbl.setText(rightAnswerTemplate +
                template.getDiagnosis().toLocalizedString());
            binder.setBean(examination);
        }
        
        
    }
    
    public void makeAnswersVisible(boolean visible) {
        rightStateLbl.setVisible(true);
        //rightStateLbl.setText("<div style=\"color=red>");
        rightRiskFactorsLbl.setVisible(visible);
        rightVaccinationEffectLbl.setVisible(visible);
        rightContactPeopleLbl.setVisible(visible);
        rightDiaskinTestLbl.setVisible(visible);
        rightFocusCategoryLbl.setVisible(visible);
        rightFocusIsPresentLbl.setVisible(visible);
        rightNeedsPhthisiologistLbl.setVisible(visible);
        rightDiagnosisLbl.setVisible(visible);
        rightPpdLbl.setVisible(visible);
        rightXRayDiagramSyndromeLbl.setVisible(visible);
        rightInfectingAgeLbl.setVisible(visible);
        rightImmuneDiagnosticLbl.setVisible(visible);
        percentsAndMark.setVisible(visible);
    }
    
    protected void setExamination(Examination examination) {
        this.examination = examination;
    }
    
    protected Examination getExamination() {
        return this.examination;
    }
    
    public Examination getTemplate() {
        return template;
    }
    
    public void setTemplate(Examination template) {
        this.template = template;
    }
    
    protected void setAndShowPicture() {
        StreamResource resource = new StreamResource("Картинка",
            () -> new ByteArrayInputStream(getExamination().getImageContent()));
        picture.setSrc(resource);
        picture.setWidth("500px");
        picture.setHeight("600px");
        picture.setVisible((getExamination().getImageContent() != null &&
            getExamination().getImageContent().length > 0));
    }
    
    protected boolean isExaminationFilled(Examination examination) {
        boolean result = false;
        result =
            examination.getContactPeople() != null &&
                examination.getState() != null &&
                examination.getDiagnosis() != null &&
                examination.getFocusCategory() != null &&
                examination.getFocusIsPresent() != null &&
                examination.getNeedsPhthisiologist() != null &&
                examination.getInfectingAge() != null &&
                examination.getImmuneDiagnostic() != null &&
                examination.getDiaskinTest() != null &&
                examination.getPpd() != null &&
                examination.getVaccinationEffect() != null &&
                !CollectionUtils.isEmpty(examination.getRiskFactors()) &&
                examination.getXRayDiagramSyndrome() != null;
        return result;
        
    }

}
