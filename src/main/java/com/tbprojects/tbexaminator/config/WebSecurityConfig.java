package com.tbprojects.tbexaminator.config;

import com.tbprojects.tbexaminator.vaadinsecurity.CustomRequestCache;
import com.tbprojects.tbexaminator.vaadinsecurity.SecurityUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import javax.inject.Inject;
import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Inject
    private DataSource dataSource;
    
    private static final String LOGIN_URL = "/login";
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
        http
            
            .requestCache().requestCache(new CustomRequestCache()).and()
            
            .authorizeRequests()
            .requestMatchers(SecurityUtils::isFrameworkInternalRequest).permitAll()
            
            .anyRequest()
            .authenticated()
            
            .and()
            .formLogin().loginPage(LOGIN_URL).permitAll()
            .and().logout().logoutSuccessUrl(LOGIN_URL)
            
            .and().csrf().disable();
    }
    
    /*@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
            .passwordEncoder(getPasswordEncoder())
            .usersByUsernameQuery("select username, password, active from usr where username = ?")
            .authoritiesByUsernameQuery("select u.username, ur.roles from usr u " +
                "inner join user_role ur on u.id = ur.user_id where u.username = ? ");
    }*/
    
    
    /**
     * Allows access to static resources, bypassing Spring security.
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(
            // Vaadin Flow static resources
            "/VAADIN/**",
            
            // the standard favicon URI
            "/favicon.ico",
            
            // the robots exclusion standard
            "/robots.txt",
            
            // web application manifest
            "/manifest.webmanifest",
            "/sw.js",
            "/offline-page.html",
            
            // icons and images
            "/icons/**",
            "/images/**",
            
            // (development mode) static resources
            "/frontend/**",
            
            // (development mode) webjars
            "/webjars/**",
            
            // (development mode) H2 debugging console
            "/h2-console/**",
            
            // (production mode) static resources
            "/frontend-es5/**", "/frontend-es6/**");
    }
    
    /*@Bean
    public BCryptPasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2A, 11);
    }*/
    
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails user =
            User.withDefaultPasswordEncoder().username("user").password("user").roles("USER")
                .build();
        UserDetails admin =
            User.withDefaultPasswordEncoder().username("admin").password("admin").roles("ADMIN")
                .build();
        return new InMemoryUserDetailsManager(user, admin);
    }
}
