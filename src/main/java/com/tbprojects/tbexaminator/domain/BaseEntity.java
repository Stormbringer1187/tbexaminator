package com.tbprojects.tbexaminator.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public class BaseEntity {
    
    @Id
    @GeneratedValue
    protected UUID id;
    
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    protected Date createTs;
    protected String createdBy;
    protected Date updateTs;
    protected String updatedBy;
    protected Date deleteTs;
    protected String deletedBy;
    
    public Date getCreateTs() {
        return createTs;
    }
    
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }
    
    public String getCreatedBy() {
        return createdBy;
    }
    
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    public Date getUpdateTs() {
        return updateTs;
    }
    
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    public Date getDeleteTs() {
        return deleteTs;
    }
    
    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }
    
    public String getDeletedBy() {
        return deletedBy;
    }
    
    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }
}
