package com.tbprojects.tbexaminator.domain;

import com.tbprojects.tbexaminator.enums.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Table(name = "examination")
@Entity
public class Examination extends BaseEntity {
    
    protected String number;
    
    @ManyToOne
    protected User user;
    
    protected String studentsLastName;
    
    protected String studentsFirstName;
    
    protected String studentsMiddleName;
    
    protected Boolean isTemplate;
    
    protected byte[] imageContent;
    
    @Column(name = "description", length = 5000)
    protected String description;
    
    @Enumerated(EnumType.STRING)
    protected StateEnum state;
    
    @ElementCollection(targetClass = RiskFactorsEnum.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "riskFactors", joinColumns = @JoinColumn(name = "examination_id"))
    @Enumerated(EnumType.STRING)
    protected Set<RiskFactorsEnum> riskFactors;
    
    @Enumerated(EnumType.STRING)
    protected VaccinationEffectEnum vaccinationEffect;
    
    @Enumerated(EnumType.STRING)
    protected PPDEnum ppd;
    
    @Enumerated(EnumType.STRING)
    protected ImmuneDiagnosticEnum immuneDiagnostic;
    
    @Enumerated(EnumType.STRING)
    protected YesNoEnum diaskinTest;
    
    @Enumerated(EnumType.STRING)
    protected AgeEnum infectingAge;
    
    @Enumerated(EnumType.STRING)
    protected XRayEnum xRayDiagramSyndrome;
    
    @Enumerated(EnumType.STRING)
    protected YesNoEnum focusIsPresent;
    
    @Enumerated(EnumType.STRING)
    protected FocusEnum focusCategory;
    
    @Enumerated(EnumType.STRING)
    protected ContactPeopleEnum contactPeople;
    
    @Enumerated(EnumType.STRING)
    protected YesNoEnum needsPhthisiologist;
    
    @Enumerated(EnumType.STRING)
    protected DiagnosisEnum diagnosis;
    
    protected BigDecimal result;
    
    public BigDecimal getResult() {
        return result;
    }
    
    public void setResult(BigDecimal result) {
        this.result = result;
    }
    
    public Integer getMark() {
        return mark;
    }
    
    public void setMark(Integer mark) {
        this.mark = mark;
    }
    
    protected Integer mark;
    
    public String getNumber() {
        return number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public Boolean getIsTemplate() {
        return isTemplate;
    }
    
    public void setIsTemplate(Boolean isTemplate) {
        this.isTemplate = isTemplate;
    }
    
    public byte[] getImageContent() {
        return imageContent;
    }
    
    public void setImageContent(byte[] imageContent) {
        this.imageContent = imageContent;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public StateEnum getState() {
        return state;
    }
    
    public void setState(StateEnum state) {
        this.state = state;
    }
    
    public Set<RiskFactorsEnum> getRiskFactors() {
        return riskFactors;
    }
    
    public void setRiskFactors(Set<RiskFactorsEnum> riskFactors) {
        this.riskFactors = riskFactors;
    }
    
    public VaccinationEffectEnum getVaccinationEffect() {
        return vaccinationEffect;
    }
    
    public void setVaccinationEffect(VaccinationEffectEnum vaccinationEffect) {
        this.vaccinationEffect = vaccinationEffect;
    }
    
    public PPDEnum getPpd() {
        return ppd;
    }
    
    public void setPpd(PPDEnum ppd) {
        this.ppd = ppd;
    }
    
    public ImmuneDiagnosticEnum getImmuneDiagnostic() {
        return immuneDiagnostic;
    }
    
    public void setImmuneDiagnostic(ImmuneDiagnosticEnum immuneDiagnostic) {
        this.immuneDiagnostic = immuneDiagnostic;
    }
    
    public YesNoEnum getDiaskinTest() {
        return diaskinTest;
    }
    
    public void setDiaskinTest(YesNoEnum diaskinTest) {
        this.diaskinTest = diaskinTest;
    }
    
    public AgeEnum getInfectingAge() {
        return infectingAge;
    }
    
    public void setInfectingAge(AgeEnum infectingAge) {
        this.infectingAge = infectingAge;
    }
    
    public XRayEnum getXRayDiagramSyndrome() {
        return xRayDiagramSyndrome;
    }
    
    public void setXRayDiagramSyndrome(XRayEnum xRayDiagramSyndrome) {
        this.xRayDiagramSyndrome = xRayDiagramSyndrome;
    }
    
    public YesNoEnum getFocusIsPresent() {
        return focusIsPresent;
    }
    
    public void setFocusIsPresent(YesNoEnum focusIsPresent) {
        this.focusIsPresent = focusIsPresent;
    }
    
    public FocusEnum getFocusCategory() {
        return focusCategory;
    }
    
    public void setFocusCategory(FocusEnum focusCategory) {
        this.focusCategory = focusCategory;
    }
    
    public ContactPeopleEnum getContactPeople() {
        return contactPeople;
    }
    
    public void setContactPeople(ContactPeopleEnum contactPeople) {
        this.contactPeople = contactPeople;
    }
    
    public YesNoEnum getNeedsPhthisiologist() {
        return needsPhthisiologist;
    }
    
    public void setNeedsPhthisiologist(YesNoEnum needsPhthisiologist) {
        this.needsPhthisiologist = needsPhthisiologist;
    }
    
    public DiagnosisEnum getDiagnosis() {
        return diagnosis;
    }
    
    public void setDiagnosis(DiagnosisEnum diagnosis) {
        this.diagnosis = diagnosis;
    }
    
    public String getStudentsLastName() {
        return studentsLastName;
    }
    
    public void setStudentsLastName(String studentsLastName) {
        this.studentsLastName = studentsLastName;
    }
    
    public String getStudentsFirstName() {
        return studentsFirstName;
    }
    
    public void setStudentsFirstName(String studentsFirstName) {
        this.studentsFirstName = studentsFirstName;
    }
    
    public String getStudentsMiddleName() {
        return studentsMiddleName;
    }
    
    public void setStudentsMiddleName(String studentsMiddleName) {
        this.studentsMiddleName = studentsMiddleName;
    }
    
    public Examination createLikeThis() {
        Examination newExamination = new Examination();
        newExamination.setContactPeople(this.contactPeople);
        newExamination.setDescription(this.description);
        newExamination.setDiagnosis(this.diagnosis);
        newExamination.setDiaskinTest(this.diaskinTest);
        newExamination.setFocusCategory(this.focusCategory);
        newExamination.setFocusIsPresent(this.focusIsPresent);
        newExamination.setImageContent(this.imageContent);
        newExamination.setImmuneDiagnostic(this.immuneDiagnostic);
        newExamination.setInfectingAge(this.infectingAge);
        newExamination.setNeedsPhthisiologist(this.needsPhthisiologist);
        newExamination.setPpd(this.ppd);
        newExamination.setRiskFactors(this.riskFactors);
        newExamination.setVaccinationEffect(this.vaccinationEffect);
        newExamination.setXRayDiagramSyndrome(this.xRayDiagramSyndrome);
        newExamination.setIsTemplate(false);
        newExamination.setNumber(this.number);
        return newExamination;
    }
    
    
}
