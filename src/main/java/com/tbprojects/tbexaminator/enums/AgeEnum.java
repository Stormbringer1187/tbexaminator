package com.tbprojects.tbexaminator.enums;

public enum AgeEnum implements EnumClass<String>, LocalizedEnum {
    
    NO("N"),
    LESS_THAN_THREE("3-"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    MORE_THAN_SEVEN("7+");
    
    private String id;
    
    AgeEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static AgeEnum fromId(String id) {
        if ("N".equals(id)) {
            return NO;
        } else if ("3-".equals(id)) {
            return LESS_THAN_THREE;
        } else if ("4".equals(id)) {
            return FOUR;
        } else if ("5".equals(id)) {
            return FIVE;
        } else if ("6".equals(id)) {
            return SIX;
        } else if ("7+".equals(id)) {
            return MORE_THAN_SEVEN;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "N":
                name = "Нет";
                break;
            case "3-":
                name = "До 3-х лет";
                break;
            case "4":
                name = "4 года";
                break;
            case "5":
                name = "5 лет";
                break;
            case "6":
                name = "6 лет";
                break;
            case "7+":
                name = "7 лет и старше";
                break;
        }
        return name;
    }
}
