package com.tbprojects.tbexaminator.enums;

public enum ContactPeopleEnum implements EnumClass<String>, LocalizedEnum {
    
    ZERO("0"),
    ONE("1"),
    TWO("2"),
    THREE("3"),
    FOUR("4");
    
    private String id;
    
    ContactPeopleEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static ContactPeopleEnum fromId(String id) {
        if ("0".equals(id)) {
            return ZERO;
        } else if ("1".equals(id)) {
            return ONE;
        } else if ("2".equals(id)) {
            return TWO;
        } else if ("3".equals(id)) {
            return THREE;
        } else if ("4".equals(id)) {
            return FOUR;
        } else {
            return null;
        }
        
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "0":
                name = "0";
                break;
            case "1":
                name = "1";
                break;
            case "2":
                name = "2";
                break;
            case "3":
                name = "3";
                break;
            case "4":
                name = "4";
                break;
        }
        return name;
    }
}
