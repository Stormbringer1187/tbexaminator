package com.tbprojects.tbexaminator.enums;

public enum DiagnosisEnum implements EnumClass<String>, LocalizedEnum {
    
    HEALTHY("H"),
    EARLY("E"),
    CHILDREN_INTOXICATION("CI"),
    PRIMARY("PR"),
    INTRATHORACIC("IT"),
    MILIARY("M"),
    DISSEMINATED("D"),
    FOCAL("F"),
    INFILTRATING("I"),
    CHEESY("CH"),
    TUBERCULOMA("T"),
    CAVERNOUS("CV"),
    FIBROUS_CAVERNOUS("FC"),
    CIRRHOTIC("CR"),
    PLEURISY("PL"),
    RESIDUAL("R"),
    OTHER("O");
    
    private String id;
    
    DiagnosisEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static DiagnosisEnum fromId(String id) {
        if ("H".equals(id)) {
            return HEALTHY;
        } else if ("E".equals(id)) {
            return EARLY;
        } else if ("CI".equals(id)) {
            return CHILDREN_INTOXICATION;
        } else if ("PR".equals(id)) {
            return PRIMARY;
        } else if ("IT".equals(id)) {
            return INTRATHORACIC;
        } else if ("M".equals(id)) {
            return MILIARY;
        } else if ("D".equals(id)) {
            return DISSEMINATED;
        } else if ("F".equals(id)) {
            return FOCAL;
        } else if ("I".equals(id)) {
            return INFILTRATING;
        } else if ("CH".equals(id)) {
            return CHEESY;
        } else if ("T".equals(id)) {
            return TUBERCULOMA;
        } else if ("CV".equals(id)) {
            return CAVERNOUS;
        } else if ("FC".equals(id)) {
            return FIBROUS_CAVERNOUS;
        } else if ("CR".equals(id)) {
            return CIRRHOTIC;
        } else if ("PL".equals(id)) {
            return PLEURISY;
        } else if ("R".equals(id)) {
            return RESIDUAL;
        } else if ("O".equals(id)) {
            return OTHER;
        } else {
            return null;
        }
        
    }
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "H":
                name = "Здоров";
                break;
            case "E":
                name = "Ранний период туберкулезной инфекции";
                break;
            case "CI":
                name = "Туберкулезная интоксикация у детей и подростков";
                break;
            case "PR":
                name = "Первичный туберкулезный комплекс";
                break;
            case "IT":
                name = "Туберкулез внутригрудных лимфатических узлов";
                break;
            case "M":
                name = "Милиарный туберкулез";
                break;
            case "D":
                name = "Диссеминированный туберкулез легких";
                break;
            case "F":
                name = "Очаговый туберкулез легких";
                break;
            case "I":
                name = "Инфильтративный туберкулез легких";
                break;
            case "CH":
                name = "Казеозная пневмония";
                break;
            case "T":
                name = "Туберкулема легких";
                break;
            case "CV":
                name = "Кавернозный туберкулез легких";
                break;
            case "FC":
                name = "Фиброзно-кавернозный туберкулез легких";
                break;
            case "CR":
                name = "Цирротический туберкулез легких";
                break;
            case "PL":
                name = "Туберкулезный плеврит (в т. ч. эмпиема)";
                break;
            case "R":
                name = "Остаточные изменения после перенесенного туберкулеза (кальцинат)";
                break;
            case "O":
                name = "Другое";
                break;
        }
        return name;
    }
}
