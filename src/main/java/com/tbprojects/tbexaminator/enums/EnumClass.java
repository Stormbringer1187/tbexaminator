package com.tbprojects.tbexaminator.enums;

public interface EnumClass<T> {
    T getId();
}
