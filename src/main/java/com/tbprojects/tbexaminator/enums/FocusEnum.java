package com.tbprojects.tbexaminator.enums;

public enum FocusEnum implements EnumClass<String>, LocalizedEnum {
    
    NO("N"),
    FIRST("1"),
    SECOND("2"),
    THIRD("3"),
    FOURTH("4"),
    FIFTH("5");
    
    private String id;
    
    FocusEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static FocusEnum fromId(String id) {
        if ("N".equals(id)) {
            return NO;
        } else if ("1".equals(id)) {
            return FIRST;
        } else if ("2".equals(id)) {
            return SECOND;
        } else if ("3".equals(id)) {
            return THIRD;
        } else if ("4".equals(id)) {
            return FOURTH;
        } else if ("5".equals(id)) {
            return FIFTH;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "N":
                name = "Нет";
                break;
            case "1":
                name = "1";
                break;
            case "2":
                name = "2";
                break;
            case "3":
                name = "3";
                break;
            case "4":
                name = "4";
                break;
            case "5":
                name = "5";
                break;
        }
        return name;
    }
}
