package com.tbprojects.tbexaminator.enums;

public enum ImmuneDiagnosticEnum implements EnumClass<String>, LocalizedEnum {
    
    POST_VACCINE_ALLERGY("PVA"),
    VIRAGE("V"),
    LONG("L"),
    INCREASING("I"),
    MORE_GROWING("MG");
    
    private String id;
    
    ImmuneDiagnosticEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static ImmuneDiagnosticEnum fromId(String id) {
        if ("PVA".equals(id)) {
            return POST_VACCINE_ALLERGY;
        } else if ("V".equals(id)) {
            return VIRAGE;
        } else if ("L".equals(id)) {
            return LONG;
        } else if ("I".equals(id)) {
            return INCREASING;
        } else if ("MG".equals(id)) {
            return MORE_GROWING;
        } else {
            return null;
        }
        
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "PVA":
                name = "Поствакцинальная аллергия";
                break;
            case "V":
                name = "Вираж";
                break;
            case "L":
                name = "Длительно сохраняющаяся по годам (монотонная 12 мм и более)";
                break;
            case "I":
                name = "Нарастающая по годам";
                break;
            case "MG":
                name = "Увеличение на 6 мм и более";
                break;
        }
        return name;
    }
}
