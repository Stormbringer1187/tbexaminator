package com.tbprojects.tbexaminator.enums;

public interface LocalizedEnum {
    String toLocalizedString();
}
