package com.tbprojects.tbexaminator.enums;

public enum PPDEnum implements EnumClass<String>, LocalizedEnum {
    
    NEGATIVE("N"),
    QUESTIONABLE("Q"),
    POSITIVE("P"),
    HYPERERGIC("H");
    
    private String id;
    
    PPDEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static PPDEnum fromId(String id) {
        if ("N".equals(id)) {
            return NEGATIVE;
        } else if ("Q".equals(id)) {
            return QUESTIONABLE;
        } else if ("P".equals(id)) {
            return POSITIVE;
        } else if ("H".equals(id)) {
            return HYPERERGIC;
        } else {
            return null;
        }
        
    }
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "N":
                name = "Отрицательная";
                break;
            case "Q":
                name = "Сомнительная";
                break;
            case "P":
                name = "Положительная";
                break;
            case "H":
                name = "Гиперэргическая";
                break;
        }
        return name;
    }
}
