package com.tbprojects.tbexaminator.enums;

public enum RiskFactorsEnum implements EnumClass<String>, LocalizedEnum {
    
    NO("N"),
    EPIDEMIOLOGICAL("E"),
    BIOMEDICAL("B"),
    SOMATIC("SM"),
    SOCIAL("SC");
    
    private String id;
    
    RiskFactorsEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static RiskFactorsEnum fromId(String id) {
        if ("N".equals(id)) {
            return NO;
        } else if ("E".equals(id)) {
            return EPIDEMIOLOGICAL;
        } else if ("B".equals(id)) {
            return BIOMEDICAL;
        } else if ("SM".equals(id)) {
            return SOMATIC;
        } else if ("SC".equals(id)) {
            return SOCIAL;
        } else {
            return null;
        }
        
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "N":
                name = "Нет";
                break;
            case "E":
                name = "Эпидемиологический";
                break;
            case "B":
                name = "Медико-биологический (специфический)";
                break;
            case "SM":
                name = "Соматический";
                break;
            case "SC":
                name = "Социальный";
                break;
        }
        return name;
    }
}
