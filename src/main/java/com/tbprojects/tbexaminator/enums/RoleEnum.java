package com.tbprojects.tbexaminator.enums;

public enum RoleEnum implements EnumClass<String> {
    USER("user"),
    ADMIN("admin");
    
    private String id;
    
    RoleEnum(String id) {
        this.id = id;
    }
    
    
    @Override
    public String getId() {
        return id;
    }
    
    public RoleEnum fromId (String id) {
        switch (id) {
            case "admin":
                return ADMIN;
            default:
                return USER;
        }
    }
}
