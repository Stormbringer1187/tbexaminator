package com.tbprojects.tbexaminator.enums;

public enum SexEnum implements EnumClass<String>, LocalizedEnum {
    
    MALE("M"),
    FEMALE("F");
    
    private String id;
    
    SexEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static SexEnum fromId(String id) {
        if ("M".equals(id)) {
            return MALE;
        } else if ("F".equals(id)) {
            return FEMALE;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "M":
                name = "Мужской";
                break;
            case "F":
                name = "Женский";
                break;
        }
        return name;
    }
}
