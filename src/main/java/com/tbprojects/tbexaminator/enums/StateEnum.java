package com.tbprojects.tbexaminator.enums;

public enum StateEnum implements EnumClass<String>, LocalizedEnum {
    
    SATISFACTORY("S"),
    MODERATE("M"),
    CRITICAL("C");
    
    private String id;
    
    StateEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static StateEnum fromId(String id) {
        if ("S".equals(id)) {
            return SATISFACTORY;
        } else if ("M".equals(id)) {
            return MODERATE;
        } else if ("C".equals(id)) {
            return CRITICAL;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "S":
                name = "Удовлетворительное";
                break;
            case "M":
                name = "Средней тяжести";
                break;
            case "C":
                name = "Тяжелое";
                break;
        }
        return name;
    }
}
