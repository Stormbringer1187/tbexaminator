package com.tbprojects.tbexaminator.enums;

public enum VaccinationEffectEnum implements EnumClass<String>, LocalizedEnum {
    
    EFFECTIVE("E"),
    NOT_EFFECTIVE("NE"),
    OTHER("O");
    
    private String id;
    
    VaccinationEffectEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static VaccinationEffectEnum fromId(String id) {
        if ("E".equals(id)) {
            return EFFECTIVE;
        } else if ("NE".equals(id)) {
            return NOT_EFFECTIVE;
        } else if ("O".equals(id)) {
            return OTHER;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "E":
                name = "Эффективная";
                break;
            case "NE":
                name = "Не эффективная";
                break;
            case "O":
                name = "Другое";
                break;
        }
        return name;
    }
}
