package com.tbprojects.tbexaminator.enums;

public enum XRayEnum implements EnumClass<String>, LocalizedEnum {
    
    NORM("N"),
    CALCINE("C"),
    INCREASING("I"),
    FOCAL("F"),
    ROUND("RD"),
    LIMITED("L"),
    DISSEMINATION("D"),
    RING("RG"),
    BIAS("B");
    
    private String id;
    
    XRayEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static XRayEnum fromId(String id) {
        if ("N".equals(id)) {
            return NORM;
        } else if ("C".equals(id)) {
            return CALCINE;
        } else if ("I".equals(id)) {
            return INCREASING;
        } else if ("F".equals(id)) {
            return FOCAL;
        } else if ("RD".equals(id)) {
            return ROUND;
        } else if ("L".equals(id)) {
            return LIMITED;
        } else if ("D".equals(id)) {
            return DISSEMINATION;
        } else if ("RG".equals(id)) {
            return RING;
        } else if ("B".equals(id)) {
            return BIAS;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "N":
                name = "Норма";
                break;
            case "C":
                name = "Кальцинат";
                break;
            case "I":
                name = "Увеличение внутригрудных лимфатических узлов";
                break;
            case "F":
                name = "Очаговая тень";
                break;
            case "RD":
                name = "Округлая тень";
                break;
            case "L":
                name = "Ограниченное затемнение (инфильтрат)";
                break;
            case "D":
                name = "Диссеминация";
                break;
            case "RG":
                name = "Кольцевидная тень";
                break;
            case "B":
                name = "Смещение органов средостения";
                break;
        }
        return name;
    }
    
}
