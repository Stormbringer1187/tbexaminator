package com.tbprojects.tbexaminator.enums;

public enum YesNoEnum implements EnumClass<String>, LocalizedEnum {
    
    YES("Y"),
    NO("N");
    
    private String id;
    
    YesNoEnum(String id) {
        this.id = id;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    public static YesNoEnum fromId(String id) {
        if ("Y".equals(id)) {
            return YES;
        } else if ("N".equals(id)) {
            return NO;
        } else {
            return null;
        }
    }
    
    
    @Override
    public String toLocalizedString() {
        String name = "";
        switch (id) {
            case "Y":
                name = "Да";
                break;
            case "N":
                name = "Нет";
                break;
        }
        return name;
    }
}
