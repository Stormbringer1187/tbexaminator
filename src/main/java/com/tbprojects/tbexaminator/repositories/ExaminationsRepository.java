package com.tbprojects.tbexaminator.repositories;

import com.tbprojects.tbexaminator.domain.Examination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ExaminationsRepository extends JpaRepository<Examination, UUID> {
    @Query("from Examination e where e.number = :number and e.deleteTs is null")
    Examination findByNumber(@Param("number") String name);
    
    @Query("from Examination e where e.isTemplate = true and e.deleteTs is null")
    List<Examination> findTemplates();
    
    @Query("from Examination e where " +
        "lower(concat(e.studentsLastName, ' ', e.studentsFirstName, ' ', e.studentsMiddleName)) " +
        "like concat('%', :name, '%') and e.deleteTs is null")
    List<Examination> findByStudentsName(@Param("name") String name);
}
