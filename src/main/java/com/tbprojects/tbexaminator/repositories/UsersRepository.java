package com.tbprojects.tbexaminator.repositories;

import com.tbprojects.tbexaminator.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {
    
    @Query("from User u where lower(u.username) = lower(:username) and u.deleteTs is null")
    User findByUsername(@Param("username") String username);


}
