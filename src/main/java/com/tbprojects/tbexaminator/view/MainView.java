package com.tbprojects.tbexaminator.view;

import com.tbprojects.tbexaminator.components.ExaminationEditor;
import com.tbprojects.tbexaminator.domain.Examination;
import com.tbprojects.tbexaminator.repositories.ExaminationsRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.util.StringUtils;

import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

@PWA(name = "TB Examinator", shortName = "TBExaminator")
@Route("")
public class MainView extends VerticalLayout {
    
    protected final ExaminationsRepository examinationsRepository;
    protected ExaminationEditor examinationEditor;
    protected final Anchor logoutAnchor;
    protected final Button deleteBtn;
    protected final Label userLabel;
    protected HorizontalLayout toolbar;
    public int tasksQuantity;
    protected List<Examination> tasks;
    protected boolean isAdmin;
    protected Grid<Examination> grid;
    protected TextField filter;
   
    protected Button startBtn;
    
    @Autowired
    public MainView(ExaminationsRepository examinationsRepository, ExaminationEditor examinationEditor) {
        this.examinationsRepository = examinationsRepository;
        this.examinationEditor = examinationEditor;
    
        userLabel = new Label();
        User user =
            (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user != null) {
            userLabel.setText("Вы вошли как " + user.getUsername());
            userLabel.setWidthFull();
            for (GrantedAuthority authority : user.getAuthorities()) {
                isAdmin = StringUtils.equals(authority.getAuthority(), "ROLE_ADMIN");
            }
        }
        
        grid = new Grid<>(Examination.class);
        grid.setHeight("300px");
        grid.removeAllColumns();
        grid.addColumn(Examination::getCreateTs).setHeader("Дата и время создания");
        grid.addColumn(Examination::getNumber).setHeader("Номер задания");
        grid.addColumn(Examination::getStudentsLastName).setHeader("Фамилия студента");
        grid.addColumn(Examination::getStudentsFirstName).setHeader("Имя студента");
        grid.addColumn(Examination::getStudentsMiddleName).setHeader("Отчество студента");
        grid.addComponentColumn(value -> createIconFromBoolean(value.getIsTemplate()))
            .setHeader("Является шаблоном");
        grid.addColumn(Examination::getResult).setHeader("Результат");
        grid.addColumn(Examination::getMark).setHeader("Оценка");
        grid.addItemDoubleClickListener(click -> {
            examinationEditor.editExamination(click.getItem(), null, isAdmin);
            grid.select(click.getItem());
        });
        grid.setColumnReorderingAllowed(true);
        grid.setMultiSort(true);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        
        filter = new TextField();
        filter.setPlaceholder("Начните вводить для поиска...");
        filter.setWidth("400px");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.setVisible(isAdmin);
        filter.addValueChangeListener(e -> {
           showExaminationBrowser(isAdmin, e.getValue());
        });
        
        initializeTemplates();
        
        startBtn = new Button("Начать");
        startBtn.setEnabled(true);
        deleteBtn = new Button("Удалить");
        logoutAnchor = new Anchor("/logout", "Выйти");
        
        toolbar = new HorizontalLayout();
        toolbar.setWidth("500px");
        toolbar.add(startBtn, filter, deleteBtn);
        
        startBtn.addClickListener(e -> {
            showExamination();
        });
        examinationEditor.setChangeHandler(() -> {
            showExaminationBrowser(isAdmin, null);
            initializeTemplates();
            startBtn.setEnabled(true);
        });
        startBtn.setWidth("300px");
        
        deleteBtn.addClickListener(e -> {
            Set<Examination> examinations = grid.getSelectedItems();
            if (!CollectionUtils.isEmpty(examinations)) {
                for (Examination examination : examinations) {
                    examination.setDeleteTs(Date.from(Instant.now()));
                    examination.setDeletedBy(user != null ? user.getUsername() : null);
                    examinationsRepository.save(examination);
                }
                showExaminationBrowser(isAdmin, null);
            }
        });
        deleteBtn.setVisible(isAdmin);
        deleteBtn.setWidth("300px");
        
        add(userLabel, logoutAnchor, toolbar, grid, examinationEditor);
        setHorizontalComponentAlignment(Alignment.END, logoutAnchor);
        setHorizontalComponentAlignment(Alignment.START, userLabel);
        showExaminationBrowser(isAdmin, null);
        
    }
    
    protected void showExaminationBrowser(boolean isAdmin, String name) {
        if (isAdmin) {
            if (StringUtils.isEmpty(name)) {
                grid.setItems(examinationsRepository.findByStudentsName(""));
            } else {
                grid.setItems(examinationsRepository
                    .findByStudentsName(StringUtils.toLowerCase(name, Locale.getDefault())));
            }
        }
        grid.setVisible(isAdmin);
        
    }
    
    protected void showExamination() {
        startBtn.setEnabled(false);
        Examination examTask = null;
        Examination examination = new Examination();
        if (!isAdmin) {
            Random random = new Random(System.currentTimeMillis());
            int number = random.nextInt(tasksQuantity) + 1;
            for (Examination t : tasks) {
                if (StringUtils.equals(t.getNumber(), String.valueOf(number))) {
                    examTask = t;
                    break;
                }
            }
        }
        examinationEditor.editExamination(examination, examTask, isAdmin);
        
    }
    
    protected Icon createIconFromBoolean(boolean bValue) {
        if (bValue) {
            return VaadinIcon.CHECK_SQUARE_O.create();
        } else {
            return VaadinIcon.THIN_SQUARE.create();
        }
    }
    
    protected void initializeTemplates() {
        tasks = examinationsRepository.findTemplates();
        tasksQuantity = tasks.size();
    }
    
}
